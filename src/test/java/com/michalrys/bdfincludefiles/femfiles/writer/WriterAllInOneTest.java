package com.michalrys.bdfincludefiles.femfiles.writer;

import com.michalrys.bdfincludefiles.femfiles.file.ExtendedFile;
import com.michalrys.bdfincludefiles.femfiles.file.FileContainer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

public class WriterAllInOneTest {
    private final static String FILE_PATH_INPUT_FILE = ".\\src\\resources\\testdata\\MainFile.bdf";
    private static final String POSTFIX = "_MyDefinedPostfix";
    private final static String FILE_PATH_OUT_FILE = ".\\src\\resources\\testdata\\MainFile" + POSTFIX + ".bdf";
    private Writer writer;

    @Before
    public void setUp() {
        writer = new WriterAllInOne(POSTFIX);
    }

    @Test
    public void shouldWriteDataToAFileIncludingFilenamePostfix() throws IOException {
        //given
        FileContainer file = new ExtendedFile(FILE_PATH_INPUT_FILE);

        //when
        writer.write(getExampleDataFile(), file.getFolderPath(), file.getFileName());

        //then
        File newFile = new File(FILE_PATH_OUT_FILE);
        Assert.assertTrue(newFile.exists());
        newFile.delete();
    }

    @Test
    public void shouldWriteCorrectlyGivenData() throws IOException {
        //given
        FileContainer file = new ExtendedFile(FILE_PATH_INPUT_FILE);
        writer.write(getExampleDataFile(), file.getFolderPath(), file.getFileName());
        File newFile = new File(FILE_PATH_OUT_FILE);

        //when
        StringBuffer newFileRead = readFile(newFile);

        //then
        Assert.assertEquals(getExampleDataFile().toString(), newFileRead.toString());
        newFile.delete();
    }

    private StringBuffer readFile(File file) {
        StringBuffer results = new StringBuffer();
        String line;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            while ((line = bufferedReader.readLine()) != null) {
                results.append(line + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        results.replace(results.length() - 1, results.length(), "");
        return results;
    }

    private StringBuffer getExampleDataFile() {
        StringBuffer result = new StringBuffer();
        result.append("" +
                "PARAM,PRGPST,NO\n" +
                "$\n" +
                "$  GRID Data\n" +
                "$\n" +
                "INCLUDE '01_MESH_1_ALL.bdf'\n" +
                "INCLUDE '01_MESH_2_ADDITIONAL.bdf'\n" +
                "$\n" +
                "ENDDATA");

        return result;
    }
}