package com.michalrys.bdfincludefiles.femfiles.file;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class ExtendedFileTest {

    private FileContainer file;

    private final static String FILE_1_PATH_CORRECT_FOR_JAVA = "d:/src/resources/testdata/01_LC_STA.bdf";
    private final static String FILE_1_PATH_CORRECT_FOR_WINDOWS = "d:\\src\\resources\\testdata\\01_LC_STA.bdf";
    private final static String FILE_1_NAME_ONLY = "01_LC_STA.bdf";
    private final static String FILE_2_PATH = ".\\src\\resources\\testdata\\01_LC_STA.bdf";

    @Test
    public void shouldSetCorrectFilePathForGivenFilePathInWindowsStyle() {
        //given
        file = new ExtendedFile(FILE_1_PATH_CORRECT_FOR_WINDOWS);
        //when
        String result = file.getFilePath();
        //then
        Assert.assertEquals(new File(FILE_1_PATH_CORRECT_FOR_JAVA), new File(result));
    }

    @Test
    public void shouldReturnFileName() {
        //given
        file = new ExtendedFile(FILE_1_PATH_CORRECT_FOR_WINDOWS);
        //when
        String resultFromFemFile = file.getFileName();
        //then
        Assert.assertEquals(FILE_1_NAME_ONLY, resultFromFemFile);
    }

    @Test
    public void shouldReturnPathToFolderOnly() {
        //given
        String expectedResult = "d:\\src\\resources\\testdata\\";
        file = new ExtendedFile(FILE_1_PATH_CORRECT_FOR_WINDOWS);
        //when
        String resultFromFemFile = file.getFolderPath();
        //then
        Assert.assertEquals(expectedResult, resultFromFemFile);
    }

    @Test
    public void shouldDeleteFile() throws IOException {
        //given
        File fileExisting = new File(FILE_2_PATH);
        fileExisting.createNewFile();

        FileContainer file = new ExtendedFile(FILE_2_PATH);
        //when
        file.deleteFile();

        //then
        Assert.assertFalse(fileExisting.exists());
    }

}