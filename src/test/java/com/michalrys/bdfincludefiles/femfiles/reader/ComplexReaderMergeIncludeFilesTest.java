package com.michalrys.bdfincludefiles.femfiles.reader;

import com.michalrys.bdfincludefiles.femfiles.file.ExtendedFile;
import com.michalrys.bdfincludefiles.femfiles.file.FileContainer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

public class ComplexReaderMergeIncludeFilesTest {

    private final static String FILE_PATH_NOT_EXISTING_FILE = ".\\src\\resources\\testdata\\MainFile.bdf";
    private final static String FILE_PATH_MAIN_FILE = ".\\src\\resources\\testdata\\01_LC_STA_TEST.bdf";
    private final static String FILE_PATH_INCLUDE_FILE1 = ".\\src\\resources\\testdata\\01_MESH_1_ALL.bdf";
    private final static String FILE_PATH_INCLUDE_FILE2 = ".\\src\\resources\\testdata\\01_MESH_2_ADDITIONAL.bdf";

    private SimpleReader simpleReader;
    private Reader complexReader;

    @Before
    public void setUp() {
        simpleReader = new SimpleReaderImpl();
        complexReader = new ComplexReaderMergeIncludeFiles(simpleReader);
    }

    @Test
    public void shouldReadAllIncludes() throws IOException {
        // given
        FileContainer fileMainFile = new ExtendedFile(FILE_PATH_MAIN_FILE);
        createFileWithContent(fileMainFile, getExampleDataMainFile());

        FileContainer fileIncludeFileA = new ExtendedFile(FILE_PATH_INCLUDE_FILE1);
        createFileWithContent(fileIncludeFileA, getExampleDataIncludeFile1());

        FileContainer fileIncludeFileB = new ExtendedFile(FILE_PATH_INCLUDE_FILE2);
        createFileWithContent(fileIncludeFileB, getExampleDataIncludeFile2());

        // when
        StringBuffer completeContent = complexReader.get(fileMainFile.getFolderPath(), fileMainFile.getFileName());

        // then
        Assert.assertEquals(getExampleDataMainFileWithMergedIncludes(), completeContent.toString());
        fileMainFile.deleteFile();
        fileIncludeFileA.deleteFile();
        fileIncludeFileB.deleteFile();
    }

    @Test(expected = IOException.class)
    public void shouldReturnExceptionForNotExistingMainFile() throws IOException {
        //given
        FileContainer notExistingFile = new ExtendedFile(FILE_PATH_NOT_EXISTING_FILE);

        //when
        StringBuffer content = complexReader.get(notExistingFile.getFolderPath(), notExistingFile.getFileName());

        //then
        Assert.assertEquals(null, content);
    }

    private void createFileWithContent(FileContainer fileExampleData, String dataToSave) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileExampleData.getFilePath()))) {
            bufferedWriter.write(dataToSave);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getExampleDataMainFile() {
        return "" +
                "PARAM,PRGPST,NO\n" +
                "$\n" +
                "$  GRID Data\n" +
                "$\n" +
                "INCLUDE '01_MESH_1_ALL.bdf'\n" +
                "INCLUDE '01_MESH_2_ADDITIONAL.bdf'\n" +
                "$\n" +
                "ENDDATA";
    }

    private String getExampleDataIncludeFile1() {
        return "" +
                "$$\n" +
                "$$  GRID Data\n" +
                "$$\n" +
                "GRID           1        .9999997-1000.0 -610.0\n" +
                "GRID           2        .99999971000.0  -610.0\n" +
                "GRID           3        -224.692-1.41-12-252.5\n" +
                "GRID           4        2.457-12-114.0  -530.0\n" +
                "GRID           5        7.269-13114.0   -530.0\n" +
                "GRID           6        740.0   -2.38-12-514.5\n" +
                "GRID           7        -740.0  -1.27-11-514.5\n" +
                "GRID          11        1149.998-750.0  -434.5\n" +
                "GRID          12        1149.998750.0   -434.5\n" +
                "GRID          21        -1150.0 -750.0  -434.5\n" +
                "GRID          22        -1150.0 750.0   -434.5\n" +
                "GRID          31        400.0   -1227.5 -435.0\n" +
                "GRID          32        400.0   1227.5  -435.0\n" +
                "GRID          41        8.086-12-549.825-278.502\n" +
                "GRID          42        4.69-13 549.825 -278.502\n" +
                "GRID          51        439.3557-1357.5 -515.031\n" +
                "GRID          52        438.018 1358.0  -515.171\n" +
                "GRID          70        0.0     0.0     -2092.48\n" +
                "GRID          91        2269.5  -743.0  -80.1001\n" +
                "GRID          92        2269.5  742.9998-80.1001\n" +
                "GRID          95        400.0   605.0   -1119.0\n" +
                "GRID          96        -400.0  -605.0  -1119.0";
    }

    private String getExampleDataIncludeFile2() {
        return "" +
                "RBE3      807910           35017  1234561.0          123   48313   52250+       \n" +
                "+          52251   52252   52253   52254   52255   52256   52257   52258+       \n" +
                "+          52259   52260   52261   52262   52263   52264   52265   52266+       \n" +
                "+          52267   52268   52269   52270   52271   52272   52476   52477+       \n" +
                "+          52478   52479   52480   52481   52482   52483   52484   52485+       \n" +
                "+          52486\n" +
                "RBE3      807911           35018  1234561.0          123   48316   52296+       \n" +
                "+          52297   52298   52299   52300   52301   52302   52303   52304+       \n" +
                "+          52305   52306   52307   52308   52309   52310   52311   52312+       \n" +
                "+          52313   52314   52315   52316   52317   52318   52498   52499+       \n" +
                "+          52500   52501   52502   52503   52504   52505   52506   52507+       \n" +
                "+          52508\n" +
                "$\n" +
                "$HMMOVE      237\n" +
                "$         807902THRU      807911\n" +
                "$";
    }

    private String getExampleDataMainFileWithMergedIncludes() {
        return "" +
                "PARAM,PRGPST,NO\n" +
                "$\n" +
                "$  GRID Data\n" +
                "$\n" +
                "$$\n" +
                "$$  GRID Data\n" +
                "$$\n" +
                "GRID           1        .9999997-1000.0 -610.0\n" +
                "GRID           2        .99999971000.0  -610.0\n" +
                "GRID           3        -224.692-1.41-12-252.5\n" +
                "GRID           4        2.457-12-114.0  -530.0\n" +
                "GRID           5        7.269-13114.0   -530.0\n" +
                "GRID           6        740.0   -2.38-12-514.5\n" +
                "GRID           7        -740.0  -1.27-11-514.5\n" +
                "GRID          11        1149.998-750.0  -434.5\n" +
                "GRID          12        1149.998750.0   -434.5\n" +
                "GRID          21        -1150.0 -750.0  -434.5\n" +
                "GRID          22        -1150.0 750.0   -434.5\n" +
                "GRID          31        400.0   -1227.5 -435.0\n" +
                "GRID          32        400.0   1227.5  -435.0\n" +
                "GRID          41        8.086-12-549.825-278.502\n" +
                "GRID          42        4.69-13 549.825 -278.502\n" +
                "GRID          51        439.3557-1357.5 -515.031\n" +
                "GRID          52        438.018 1358.0  -515.171\n" +
                "GRID          70        0.0     0.0     -2092.48\n" +
                "GRID          91        2269.5  -743.0  -80.1001\n" +
                "GRID          92        2269.5  742.9998-80.1001\n" +
                "GRID          95        400.0   605.0   -1119.0\n" +
                "GRID          96        -400.0  -605.0  -1119.0\n" +
                "RBE3      807910           35017  1234561.0          123   48313   52250+       \n" +
                "+          52251   52252   52253   52254   52255   52256   52257   52258+       \n" +
                "+          52259   52260   52261   52262   52263   52264   52265   52266+       \n" +
                "+          52267   52268   52269   52270   52271   52272   52476   52477+       \n" +
                "+          52478   52479   52480   52481   52482   52483   52484   52485+       \n" +
                "+          52486\n" +
                "RBE3      807911           35018  1234561.0          123   48316   52296+       \n" +
                "+          52297   52298   52299   52300   52301   52302   52303   52304+       \n" +
                "+          52305   52306   52307   52308   52309   52310   52311   52312+       \n" +
                "+          52313   52314   52315   52316   52317   52318   52498   52499+       \n" +
                "+          52500   52501   52502   52503   52504   52505   52506   52507+       \n" +
                "+          52508\n" +
                "$\n" +
                "$HMMOVE      237\n" +
                "$         807902THRU      807911\n" +
                "$\n" +
                "$\n" +
                "ENDDATA";
    }
}