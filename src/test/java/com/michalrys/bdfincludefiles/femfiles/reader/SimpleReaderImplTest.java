package com.michalrys.bdfincludefiles.femfiles.reader;

import com.michalrys.bdfincludefiles.femfiles.file.ExtendedFile;
import com.michalrys.bdfincludefiles.femfiles.file.FileContainer;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class SimpleReaderImplTest {

    private final static String FILE_PATH_CORRECT = ".\\src\\resources\\testdata\\01_LC_STA_TEST.bdf";
    private final static String FILE_PATH_NOT_EXISTING_FILE = ".\\src\\resources\\testdata\\MainFile.bdf";

    @Test
    public void shouldReturnContentFileForGivenFolderPathAndFileName() throws IOException {
        //given
        FileContainer file = new ExtendedFile(FILE_PATH_CORRECT);
        createFileWithContent(file, getExampleData());
        Reader simpleReader = new SimpleReaderImpl();

        //when
        StringBuffer fileContent = simpleReader.get(file.getFolderPath(), file.getFileName());

        //then
        Assert.assertEquals(getExampleData(), fileContent.toString());
        file.deleteFile();
    }

    @Test(expected = IOException.class)
    public void shouldNotReadFileWhenDoesNotExistAndReturnException() throws IOException {
        //given
        FileContainer file = new ExtendedFile(FILE_PATH_NOT_EXISTING_FILE);
        Reader simpleReader = new SimpleReaderImpl();

        //when
        StringBuffer fileContent = simpleReader.get(file.getFolderPath(), file.getFileName());

        //then
        Assert.assertEquals(null, fileContent);
    }


    private void createFileWithContent(FileContainer fileExampleData, String dataToSave) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileExampleData.getFilePath()))) {
            bufferedWriter.write(dataToSave);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getExampleData() {
        return "" +
                "PARAM,PRGPST,NO\n" +
                "$\n" +
                "$  GRID Data\n" +
                "$\n" +
                "INCLUDE '01_MESH_1_ALL.bdf'\n" +
                "INCLUDE '01_MESH_2_ADDITIONAL.bdf'\n" +
                "$\n" +
                "ENDDATA";
    }
}