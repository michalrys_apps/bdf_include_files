package com.michalrys.bdfincludefiles.femfiles;

import com.michalrys.bdfincludefiles.femfiles.file.ExtendedFile;
import com.michalrys.bdfincludefiles.femfiles.file.FileContainer;
import com.michalrys.bdfincludefiles.femfiles.reader.ComplexReaderMergeIncludeFiles;
import com.michalrys.bdfincludefiles.femfiles.reader.ComplexReaderMergeIncludeFilesTest;
import com.michalrys.bdfincludefiles.femfiles.writer.WriterAllInOne;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.io.*;

import static org.mockito.Mockito.*;

public class AnyFemFilesTest {

    private FEMFile femFile;
    private final static String FILE_PATH_DUMMY = "dummy_file_path";
    private final static String FILE_PATH_MAIN_FILE = ".\\src\\resources\\testdata\\01_LC_STA_TEST.bdf";
    private final static String FILE_PATH_NEW_CREATED_FILE_WITH_MERGED_INLCUDES = ".\\src\\resources\\testdata\\01_LC_STA_TEST_AllInOne.bdf";
    private final static String FILE_PATH_INCLUDE_FILE1 = ".\\src\\resources\\testdata\\01_MESH_1_ALL.bdf";
    private final static String FILE_PATH_INCLUDE_FILE2 = ".\\src\\resources\\testdata\\01_MESH_2_ADDITIONAL.bdf";
    private final static String FILE_PATH_MAIN_NOT_EXISTING_FILE = "x:\\not_existing_file.error";

    private final static String MESSAGE_FOR_FILE_READING_EXCEPTION = "$ !!!ERROR: There was problem with reading a file: ";
    private final static String MESSAGE_FOR_FILE_CORRECT_WRITING = "File was written successfully.";
    private final static String MESSAGE_FOR_FILE_WRITING_EXCEPTION = "There was problem with writing a file. Please check it.";
    private final static String MESSAGE_FOR_FILE_READING_IO_EXCEPTION = "There was problem with I/O file operation.";

    @Test
    public void shouldReadAllIncludesForGivenFilePath() {
        // given
        File fileMainFile = new File(FILE_PATH_MAIN_FILE);
        createFileWithContent(fileMainFile, getExampleDataMainFile());

        File fileIncludeFileA = new File(FILE_PATH_INCLUDE_FILE1);
        createFileWithContent(fileIncludeFileA, getExampleDataIncludeFile1());

        File fileIncludeFileB = new File(FILE_PATH_INCLUDE_FILE2);
        createFileWithContent(fileIncludeFileB, getExampleDataIncludeFile2());

        femFile = new AnyFemFile(FILE_PATH_MAIN_FILE);
        // when
        StringBuffer completeContent = femFile.readCompleteContent();

        // then
        Assert.assertEquals(getExampleDataMainFileWithMergedIncludes(), completeContent.toString());
        fileMainFile.delete();
        fileIncludeFileA.delete();
        fileIncludeFileB.delete();
    }

    @Test
    public void shouldReadAllIncludesForGivenFile() {
        // given
        File fileMainFile = new File(FILE_PATH_MAIN_FILE);
        createFileWithContent(fileMainFile, getExampleDataMainFile());

        File fileIncludeFileA = new File(FILE_PATH_INCLUDE_FILE1);
        createFileWithContent(fileIncludeFileA, getExampleDataIncludeFile1());

        File fileIncludeFileB = new File(FILE_PATH_INCLUDE_FILE2);
        createFileWithContent(fileIncludeFileB, getExampleDataIncludeFile2());

        femFile = new AnyFemFile(fileMainFile);
        // when
        StringBuffer completeContent = femFile.readCompleteContent();

        // then
        Assert.assertEquals(getExampleDataMainFileWithMergedIncludes(), completeContent.toString());
        fileMainFile.delete();
        fileIncludeFileA.delete();
        fileIncludeFileB.delete();
    }

    @Test
    public void shouldReturnTextMessageOnlyWhenReadAllIncludesAndMainFileDoesNotExist() {
        //given
        femFile = new AnyFemFile(FILE_PATH_MAIN_NOT_EXISTING_FILE);
        FileContainer file = new ExtendedFile(FILE_PATH_MAIN_NOT_EXISTING_FILE);

        //when
        StringBuffer results = femFile.readCompleteContent();

        //then
        Assert.assertEquals(MESSAGE_FOR_FILE_READING_EXCEPTION + file.getFileName(), results.toString());
    }

    @Test
    public void shouldReturnTextMessageOnlyWhenReadAllIncludesAndSomeIncludeFileDoesNotExist() {
        //given
        File fileMainFile = new File(FILE_PATH_MAIN_FILE);
        createFileWithContent(fileMainFile, getExampleDataMainFile());

        File fileIncludeFileA = new File(FILE_PATH_INCLUDE_FILE1);
        createFileWithContent(fileIncludeFileA, getExampleDataIncludeFile1());

        femFile = new AnyFemFile(FILE_PATH_MAIN_FILE);

        //when
        StringBuffer results = femFile.readCompleteContent();

        //then
        Assert.assertEquals(getExampleDataMainFileWithPartialMergedIncludes1(), results.toString());
        fileMainFile.delete();
        fileIncludeFileA.delete();
    }

    @Test
    public void shouldReturnTextMessageOnlyWhenReadAllIncludesAndSomeIncludeFileDoesNotExist2() {
        //given
        File fileMainFile = new File(FILE_PATH_MAIN_FILE);
        createFileWithContent(fileMainFile, getExampleDataMainFile());

        File fileIncludeFileB = new File(FILE_PATH_INCLUDE_FILE2);
        createFileWithContent(fileIncludeFileB, getExampleDataIncludeFile2());

        femFile = new AnyFemFile(FILE_PATH_MAIN_FILE);

        //when
        StringBuffer results = femFile.readCompleteContent();

        //then
        Assert.assertEquals(getExampleDataMainFileWithPartialMergedIncludes2(), results.toString());
        fileMainFile.delete();
        fileIncludeFileB.delete();
    }

    @Test
    public void shouldWriteToANewFileMergedContentOfMainFileAndIncludeFiles() {
        //given
        File fileMainFile = new File(FILE_PATH_MAIN_FILE);
        createFileWithContent(fileMainFile, getExampleDataMainFile());

        File fileIncludeFileA = new File(FILE_PATH_INCLUDE_FILE1);
        createFileWithContent(fileIncludeFileA, getExampleDataIncludeFile1());

        File fileIncludeFileB = new File(FILE_PATH_INCLUDE_FILE2);
        createFileWithContent(fileIncludeFileB, getExampleDataIncludeFile2());

        femFile = new AnyFemFile(fileMainFile);
        StringBuffer completeContent = femFile.readCompleteContent();

        //when
        femFile.saveCompleteContentToNewFile(completeContent);

        //then
        File fileNewToSave = new File(FILE_PATH_NEW_CREATED_FILE_WITH_MERGED_INLCUDES);
        Assert.assertTrue(fileNewToSave.exists());

        StringBuffer readContent = readFile(fileNewToSave);
        Assert.assertEquals(getExampleDataMainFileWithMergedIncludes(), readContent.toString());

        fileMainFile.delete();
        fileIncludeFileA.delete();
        fileIncludeFileB.delete();
        fileNewToSave.delete();
    }

    @Test
    public void shouldSetContent() {
        //given
        File fileMainFile = new File(FILE_PATH_MAIN_FILE);
        createFileWithContent(fileMainFile, getExampleDataMainFile());

        File fileIncludeFileA = new File(FILE_PATH_INCLUDE_FILE1);
        createFileWithContent(fileIncludeFileA, getExampleDataIncludeFile1());

        File fileIncludeFileB = new File(FILE_PATH_INCLUDE_FILE2);
        createFileWithContent(fileIncludeFileB, getExampleDataIncludeFile2());

        femFile = new AnyFemFile(FILE_PATH_MAIN_FILE);
        // when
        femFile.set();

        // then
        Assert.assertEquals(getExampleDataMainFileWithMergedIncludes(), femFile.get().toString());
        fileMainFile.delete();
        fileIncludeFileA.delete();
        fileIncludeFileB.delete();
    }

    @Test
    public void shouldGetContent() {
        //given
        File fileMainFile = new File(FILE_PATH_MAIN_FILE);
        createFileWithContent(fileMainFile, getExampleDataMainFile());

        File fileIncludeFileA = new File(FILE_PATH_INCLUDE_FILE1);
        createFileWithContent(fileIncludeFileA, getExampleDataIncludeFile1());

        File fileIncludeFileB = new File(FILE_PATH_INCLUDE_FILE2);
        createFileWithContent(fileIncludeFileB, getExampleDataIncludeFile2());

        femFile = new AnyFemFile(FILE_PATH_MAIN_FILE);
        femFile.set();
        // when
        StringBuffer results = femFile.get();

        // then
        Assert.assertEquals(getExampleDataMainFileWithMergedIncludes(), results.toString());
        fileMainFile.delete();
        fileIncludeFileA.delete();
        fileIncludeFileB.delete();
    }

    @Test
    public void shouldSaveContent() {
        //given
        File fileMainFile = new File(FILE_PATH_MAIN_FILE);
        createFileWithContent(fileMainFile, getExampleDataMainFile());

        File fileIncludeFileA = new File(FILE_PATH_INCLUDE_FILE1);
        createFileWithContent(fileIncludeFileA, getExampleDataIncludeFile1());

        File fileIncludeFileB = new File(FILE_PATH_INCLUDE_FILE2);
        createFileWithContent(fileIncludeFileB, getExampleDataIncludeFile2());

        femFile = new AnyFemFile(fileMainFile);
        femFile.set();

        //when
        femFile.save();

        //then
        File fileNewToSave = new File(FILE_PATH_NEW_CREATED_FILE_WITH_MERGED_INLCUDES);
        Assert.assertTrue(fileNewToSave.exists());

        StringBuffer readContent = readFile(fileNewToSave);
        Assert.assertEquals(getExampleDataMainFileWithMergedIncludes(), readContent.toString());

        fileMainFile.delete();
        fileIncludeFileA.delete();
        fileIncludeFileB.delete();
        fileNewToSave.delete();
    }

    @Test
    public void shouldReturnMessageThatSavedSuccessfully() {
        //given
        File fileMainFile = new File(FILE_PATH_MAIN_FILE);
        createFileWithContent(fileMainFile, getExampleDataMainFile());

        File fileIncludeFileA = new File(FILE_PATH_INCLUDE_FILE1);
        createFileWithContent(fileIncludeFileA, getExampleDataIncludeFile1());

        File fileIncludeFileB = new File(FILE_PATH_INCLUDE_FILE2);
        createFileWithContent(fileIncludeFileB, getExampleDataIncludeFile2());

        femFile = new AnyFemFile(fileMainFile);
        femFile.set();

        //when
        String saveMessage = femFile.save();

        //then
        File fileNewToSave = new File(FILE_PATH_NEW_CREATED_FILE_WITH_MERGED_INLCUDES);

        Assert.assertEquals(MESSAGE_FOR_FILE_CORRECT_WRITING, saveMessage);

        fileMainFile.delete();
        fileIncludeFileA.delete();
        fileIncludeFileB.delete();
        fileNewToSave.delete();
    }

    @Test
    public void shouldReturnMessageThatDidNotSave() throws IOException {
        //given
        WriterAllInOne writerMock = mock(WriterAllInOne.class);
        doAnswer((i) -> {
            throw new IOException();
        }).when(writerMock).write(ArgumentMatchers.any(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
        femFile = new AnyFemFile(FILE_PATH_DUMMY);
        femFile.setWriter(writerMock);

        //when
        String saveMessage = femFile.save();

        //then
        Assert.assertEquals(MESSAGE_FOR_FILE_WRITING_EXCEPTION, saveMessage);
    }

    @Test
    public void shouldReturnMessageOnlyWhenIOExceptionOccursDuringFileReading() throws IOException {
        //given
        ComplexReaderMergeIncludeFiles readerMock = mock(ComplexReaderMergeIncludeFiles.class);
        doAnswer((i) -> {
            throw new IOException();
        }).when(readerMock).get(ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
        femFile = new AnyFemFile(FILE_PATH_DUMMY);
        femFile.setReader(readerMock);
        femFile.set();

        //when
        StringBuffer contentResult = femFile.get();

        //then
        Assert.assertEquals(MESSAGE_FOR_FILE_READING_IO_EXCEPTION, contentResult.toString());

    }

    private void createFileWithContent(File fileExampleData, String dataToSave) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileExampleData))) {
            bufferedWriter.write(dataToSave);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private StringBuffer readFile(File file) {
        StringBuffer results = new StringBuffer();
        String line;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            while ((line = bufferedReader.readLine()) != null) {
                results.append(line + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        results.replace(results.length() - 1, results.length(), "");
        return results;
    }

    private String getExampleDataMainFile() {
        return "" +
                "PARAM,PRGPST,NO\n" +
                "$\n" +
                "$  GRID Data\n" +
                "$\n" +
                "INCLUDE '01_MESH_1_ALL.bdf'\n" +
                "INCLUDE '01_MESH_2_ADDITIONAL.bdf'\n" +
                "$\n" +
                "ENDDATA";
    }

    private String getExampleDataIncludeFile1() {
        return "" +
                "$$\n" +
                "$$  GRID Data\n" +
                "$$\n" +
                "GRID           1        .9999997-1000.0 -610.0\n" +
                "GRID           2        .99999971000.0  -610.0\n" +
                "GRID           3        -224.692-1.41-12-252.5\n" +
                "GRID           4        2.457-12-114.0  -530.0\n" +
                "GRID           5        7.269-13114.0   -530.0\n" +
                "GRID           6        740.0   -2.38-12-514.5\n" +
                "GRID           7        -740.0  -1.27-11-514.5\n" +
                "GRID          11        1149.998-750.0  -434.5\n" +
                "GRID          12        1149.998750.0   -434.5\n" +
                "GRID          21        -1150.0 -750.0  -434.5\n" +
                "GRID          22        -1150.0 750.0   -434.5\n" +
                "GRID          31        400.0   -1227.5 -435.0\n" +
                "GRID          32        400.0   1227.5  -435.0\n" +
                "GRID          41        8.086-12-549.825-278.502\n" +
                "GRID          42        4.69-13 549.825 -278.502\n" +
                "GRID          51        439.3557-1357.5 -515.031\n" +
                "GRID          52        438.018 1358.0  -515.171\n" +
                "GRID          70        0.0     0.0     -2092.48\n" +
                "GRID          91        2269.5  -743.0  -80.1001\n" +
                "GRID          92        2269.5  742.9998-80.1001\n" +
                "GRID          95        400.0   605.0   -1119.0\n" +
                "GRID          96        -400.0  -605.0  -1119.0";
    }

    private String getExampleDataIncludeFile2() {
        return "" +
                "RBE3      807910           35017  1234561.0          123   48313   52250+       \n" +
                "+          52251   52252   52253   52254   52255   52256   52257   52258+       \n" +
                "+          52259   52260   52261   52262   52263   52264   52265   52266+       \n" +
                "+          52267   52268   52269   52270   52271   52272   52476   52477+       \n" +
                "+          52478   52479   52480   52481   52482   52483   52484   52485+       \n" +
                "+          52486\n" +
                "RBE3      807911           35018  1234561.0          123   48316   52296+       \n" +
                "+          52297   52298   52299   52300   52301   52302   52303   52304+       \n" +
                "+          52305   52306   52307   52308   52309   52310   52311   52312+       \n" +
                "+          52313   52314   52315   52316   52317   52318   52498   52499+       \n" +
                "+          52500   52501   52502   52503   52504   52505   52506   52507+       \n" +
                "+          52508\n" +
                "$\n" +
                "$HMMOVE      237\n" +
                "$         807902THRU      807911\n" +
                "$";
    }

    private String getExampleDataMainFileWithMergedIncludes() {
        return "" +
                "PARAM,PRGPST,NO\n" +
                "$\n" +
                "$  GRID Data\n" +
                "$\n" +
                "$$\n" +
                "$$  GRID Data\n" +
                "$$\n" +
                "GRID           1        .9999997-1000.0 -610.0\n" +
                "GRID           2        .99999971000.0  -610.0\n" +
                "GRID           3        -224.692-1.41-12-252.5\n" +
                "GRID           4        2.457-12-114.0  -530.0\n" +
                "GRID           5        7.269-13114.0   -530.0\n" +
                "GRID           6        740.0   -2.38-12-514.5\n" +
                "GRID           7        -740.0  -1.27-11-514.5\n" +
                "GRID          11        1149.998-750.0  -434.5\n" +
                "GRID          12        1149.998750.0   -434.5\n" +
                "GRID          21        -1150.0 -750.0  -434.5\n" +
                "GRID          22        -1150.0 750.0   -434.5\n" +
                "GRID          31        400.0   -1227.5 -435.0\n" +
                "GRID          32        400.0   1227.5  -435.0\n" +
                "GRID          41        8.086-12-549.825-278.502\n" +
                "GRID          42        4.69-13 549.825 -278.502\n" +
                "GRID          51        439.3557-1357.5 -515.031\n" +
                "GRID          52        438.018 1358.0  -515.171\n" +
                "GRID          70        0.0     0.0     -2092.48\n" +
                "GRID          91        2269.5  -743.0  -80.1001\n" +
                "GRID          92        2269.5  742.9998-80.1001\n" +
                "GRID          95        400.0   605.0   -1119.0\n" +
                "GRID          96        -400.0  -605.0  -1119.0\n" +
                "RBE3      807910           35017  1234561.0          123   48313   52250+       \n" +
                "+          52251   52252   52253   52254   52255   52256   52257   52258+       \n" +
                "+          52259   52260   52261   52262   52263   52264   52265   52266+       \n" +
                "+          52267   52268   52269   52270   52271   52272   52476   52477+       \n" +
                "+          52478   52479   52480   52481   52482   52483   52484   52485+       \n" +
                "+          52486\n" +
                "RBE3      807911           35018  1234561.0          123   48316   52296+       \n" +
                "+          52297   52298   52299   52300   52301   52302   52303   52304+       \n" +
                "+          52305   52306   52307   52308   52309   52310   52311   52312+       \n" +
                "+          52313   52314   52315   52316   52317   52318   52498   52499+       \n" +
                "+          52500   52501   52502   52503   52504   52505   52506   52507+       \n" +
                "+          52508\n" +
                "$\n" +
                "$HMMOVE      237\n" +
                "$         807902THRU      807911\n" +
                "$\n" +
                "$\n" +
                "ENDDATA";
    }

    private String getExampleDataMainFileWithPartialMergedIncludes1() {
        return "" +
                "PARAM,PRGPST,NO\n" +
                "$\n" +
                "$  GRID Data\n" +
                "$\n" +
                "$$\n" +
                "$$  GRID Data\n" +
                "$$\n" +
                "GRID           1        .9999997-1000.0 -610.0\n" +
                "GRID           2        .99999971000.0  -610.0\n" +
                "GRID           3        -224.692-1.41-12-252.5\n" +
                "GRID           4        2.457-12-114.0  -530.0\n" +
                "GRID           5        7.269-13114.0   -530.0\n" +
                "GRID           6        740.0   -2.38-12-514.5\n" +
                "GRID           7        -740.0  -1.27-11-514.5\n" +
                "GRID          11        1149.998-750.0  -434.5\n" +
                "GRID          12        1149.998750.0   -434.5\n" +
                "GRID          21        -1150.0 -750.0  -434.5\n" +
                "GRID          22        -1150.0 750.0   -434.5\n" +
                "GRID          31        400.0   -1227.5 -435.0\n" +
                "GRID          32        400.0   1227.5  -435.0\n" +
                "GRID          41        8.086-12-549.825-278.502\n" +
                "GRID          42        4.69-13 549.825 -278.502\n" +
                "GRID          51        439.3557-1357.5 -515.031\n" +
                "GRID          52        438.018 1358.0  -515.171\n" +
                "GRID          70        0.0     0.0     -2092.48\n" +
                "GRID          91        2269.5  -743.0  -80.1001\n" +
                "GRID          92        2269.5  742.9998-80.1001\n" +
                "GRID          95        400.0   605.0   -1119.0\n" +
                "GRID          96        -400.0  -605.0  -1119.0\n" +
                MESSAGE_FOR_FILE_READING_EXCEPTION + "INCLUDE '01_MESH_2_ADDITIONAL.bdf'\n" +
                "$\n" +
                "ENDDATA";
    }

    private String getExampleDataMainFileWithPartialMergedIncludes2() {
        return "" +
                "PARAM,PRGPST,NO\n" +
                "$\n" +
                "$  GRID Data\n" +
                "$\n" +
                MESSAGE_FOR_FILE_READING_EXCEPTION + "INCLUDE '01_MESH_1_ALL.bdf'\n" +
                "RBE3      807910           35017  1234561.0          123   48313   52250+       \n" +
                "+          52251   52252   52253   52254   52255   52256   52257   52258+       \n" +
                "+          52259   52260   52261   52262   52263   52264   52265   52266+       \n" +
                "+          52267   52268   52269   52270   52271   52272   52476   52477+       \n" +
                "+          52478   52479   52480   52481   52482   52483   52484   52485+       \n" +
                "+          52486\n" +
                "RBE3      807911           35018  1234561.0          123   48316   52296+       \n" +
                "+          52297   52298   52299   52300   52301   52302   52303   52304+       \n" +
                "+          52305   52306   52307   52308   52309   52310   52311   52312+       \n" +
                "+          52313   52314   52315   52316   52317   52318   52498   52499+       \n" +
                "+          52500   52501   52502   52503   52504   52505   52506   52507+       \n" +
                "+          52508\n" +
                "$\n" +
                "$HMMOVE      237\n" +
                "$         807902THRU      807911\n" +
                "$\n" +
                "$\n" +
                "ENDDATA";
    }
}
