package com.michalrys.bdfincludefiles.view.tools;

import org.junit.Assert;
import org.junit.Test;

public class WrongInfoTableSizeExceptionTest {

    @Test
    public void shouldReturnTextAndAllowableTextLength() {
        //given
        int entryLength = 10;
        String text = "Example text is too long";
        String expectedMessage = "Given text = '" + text + "' is longer than table length = '" + entryLength + "'.";
        //when
        WrongInfoTableSizeException exception = new WrongInfoTableSizeException(entryLength, text);
        //then
        Assert.assertEquals(expectedMessage, exception.getMessage());
    }
}