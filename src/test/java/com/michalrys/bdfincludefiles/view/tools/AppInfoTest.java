package com.michalrys.bdfincludefiles.view.tools;

import org.junit.Assert;
import org.junit.Test;

public class AppInfoTest {

    @Test
    public void shouldReturnDefaultHeaderForEntryLength22() {
        // given
        int entryLength = 22;
        AppInfo appInfo = new AppInfo(entryLength);
        String expectedHeader = "" +
                "+------------------------+\n" +
                "| BDF Include Files      |\n" +
                "+ - - - - - - - - - - - -+\n" +
                "| Version: 0.0.1         |\n" +
                "| Author: Michał Ryś     |\n" +
                "| Date: 2019.04.16       |\n" +
                "+------------------------+";

        // when
        String resultHeader = appInfo.getHeader();

        // then
        Assert.assertEquals(expectedHeader, resultHeader);
        System.out.println(resultHeader);
    }

    @Test
    public void shouldReturnDefaultHeaderForEntryLength50() {
        // given
        int entryLength = 50;
        AppInfo appInfo = new AppInfo(entryLength);
        String expectedHeader = "" +
                "+----------------------------------------------------+\n" +
                "| BDF Include Files                                  |\n" +
                "+ - - - - - - - - - - - - - - - - - - - - - - - - - -+\n" +
                "| Version: 0.0.1                                     |\n" +
                "| Author: Michał Ryś                                 |\n" +
                "| Date: 2019.04.16                                   |\n" +
                "+----------------------------------------------------+";

        // when
        String resultHeader = appInfo.getHeader();

        // then
        Assert.assertEquals(expectedHeader, resultHeader);
        System.out.println(resultHeader);
    }

    @Test
    public void shouldReturnDefaultHeaderForEntryLength21() {
        // given
        int entryLength = 21;
        AppInfo appInfo = new AppInfo(entryLength);
        String expectedHeader = "" +
                "+-----------------------+\n" +
                "| BDF Include Files     |\n" +
                "+ - - - - - - - - - - - +\n" +
                "| Version: 0.0.1        |\n" +
                "| Author: Michał Ryś    |\n" +
                "| Date: 2019.04.16      |\n" +
                "+-----------------------+";

        // when
        String resultHeader = appInfo.getHeader();

        // then
        Assert.assertEquals(expectedHeader, resultHeader);
        System.out.println(resultHeader);
    }

    @Test(expected = WrongInfoTableSizeException.class)
    public void shouldGiveErrorForEntryTextWhichHasUnacceptableLength() {
        //given
        int entryLength = 10;
        AppInfo appInfo = new AppInfo(entryLength);
        //when
        appInfo.getHeader();
        //then
    }

    @Test
    public void shouldReturnDefinedApplicationData() {
        // given
        int entryLength = 26;
        String title = "BDF Include Files super";
        String version = "1.0.123";
        String author = "Jan Nowak Kowalski";
        String date = "2020.12.24";

        AppInfo appInfo = new AppInfo(entryLength, title, version, author, date);
        String expectedHeader = "" +
                "+----------------------------+\n" +
                "| BDF Include Files super    |\n" +
                "+ - - - - - - - - - - - - - -+\n" +
                "| Version: 1.0.123           |\n" +
                "| Author: Jan Nowak Kowalski |\n" +
                "| Date: 2020.12.24           |\n" +
                "+----------------------------+";

        // when
        String resultHeader = appInfo.getHeader();

        // then
        Assert.assertEquals(expectedHeader, resultHeader);
        System.out.println(resultHeader);
        System.out.println("Here goes some other further text.");
    }

    @Test
    public void shouldReturnHeaderAndAdditionalRemarks() {
        // given
        int entryLength = 26;
        String title = "BDF Include Files super";
        String version = "1.0.123";
        String author = "Jan Nowak Kowalski";
        String date = "2020.12.24";

        AppInfo appInfo = new AppInfo(entryLength, title, version, author, date);
        String expectedHeader = "" +
                "+----------------------------+\n" +
                "| BDF Include Files super    |\n" +
                "+ - - - - - - - - - - - - - -+\n" +
                "| Version: 1.0.123           |\n" +
                "| Author: Jan Nowak Kowalski |\n" +
                "| Date: 2020.12.24           |\n" +
                "+----------------------------+";
        String expectedRemark = "" +
                "| Here is place for additional remarks, |\n" +
                "|  with autobreak lines, depends on the |\n" +
                "|  given length.                        |";
        String givenRemark = "Here is place for additional remarks, with autobreak lines, depends on the given length.";

        String resultHeader = appInfo.getHeader();
        // when
        String resultRemark = appInfo.getRemark(37, "|", givenRemark);

        // then
        Assert.assertEquals(expectedHeader, resultHeader);
        Assert.assertEquals(expectedRemark, resultRemark);
        System.out.println(expectedHeader);
        System.out.println(expectedRemark);
    }

    @Test
    public void shouldReturnRemarkWithLargeEntryLengthButWithShortText() {
        // given
        int entryLength = 26;
        AppInfo appInfo = new AppInfo(entryLength);

        String expectedRemark = "" +
                "| Short remark.                                      |";
        String givenRemark = "Short remark.";

        String resultHeader = appInfo.getHeader();
        // when
        String resultRemark = appInfo.getRemark(50, "|", givenRemark);

        // then
        Assert.assertEquals(expectedRemark, resultRemark);
        System.out.println(expectedRemark);
    }
}