package com.michalrys.bdfincludefiles.view.swingwindows.settings;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class JSONSettingsTest {

    private JSONSettings settings;
    private static final String KEY_AUTO_EXIT = "autoExit";
    private static final String KEY_PRINTING_ANIMATION_TIME = "printingAnimationTime";
    private final static String FILE_PATH = "d:\\BDFIncludeSettings.json";
    private final static String FILE_PATH_CONSTRUCTOR = "d:\\";
    private static final boolean DEFAULT_AUTO_EXIT = true;
    private static final int DEFAULT_PRINTING_ANIMATION_TIME = 8;

    @Before
    public void setUp() {
        settings = new JSONSettings(FILE_PATH_CONSTRUCTOR);
    }

    @Test
    public void shouldReturnFalseIfFileSettingDoesNotExist() {
        //given
        String wrongPathToFile = "this file does not exist";

        //when
        boolean result = settings.doesFileSettingsExist(wrongPathToFile);

        //then
        Assert.assertFalse(result);
    }

    @Test
    public void shouldReturnTrueWhenFileExists() throws IOException {
        //given
        String filePath = "D:\\myTestFileOnlyForThisTest.json";
        File exampleFile = new File(filePath);

        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(exampleFile))) {
            bufferedWriter.write("test only");
        } catch (IOException e) {
            e.printStackTrace();
        }

        //when
        boolean result = settings.doesFileSettingsExist(filePath);

        //then
        Assert.assertTrue(result);
        exampleFile.delete();
    }

    @Test
    public void shouldReturnDefaultSettings() {
        //given
        JSONObject results = new JSONObject();
        results.put(KEY_AUTO_EXIT, true);
        results.put(KEY_PRINTING_ANIMATION_TIME, 8);

        //when
        JSONObject settingsFromApp = settings.getDefaultSettings();

        //then
        Assert.assertEquals(results.toString(), settingsFromApp.toString());
    }

    @Test
    public void shouldReturnDefaultSettingsWhenSettingFileDoesNotExist() {
        //given
        File file = new File(FILE_PATH);
        file.delete();
        JSONObject results = new JSONObject();
        results.put(KEY_AUTO_EXIT, true);
        results.put(KEY_PRINTING_ANIMATION_TIME, 8);

        //when
        JSONObject settingsFromApp = settings.getSettings();

        //then
        Assert.assertEquals(results.toString(), settingsFromApp.toString());
    }

    @Test
    public void shouldWriteToFileDefaultSettingsWhenSettingsFileDoesNotExist() {
        //given
        JSONObject results = new JSONObject();
        File file = new File(FILE_PATH);
        file.delete();

        //when
        settings = new JSONSettings(FILE_PATH_CONSTRUCTOR);

        //then
        Assert.assertTrue(file.exists());
    }

    @Test
    public void shouldReturnTrueForValidSettingsFile() {
        //given
        File file = new File(FILE_PATH);
        file.delete();
        writeValidExampleSettingFile();
        JSONObject expectedSettings = new JSONObject();
        expectedSettings.put(KEY_AUTO_EXIT, false);
        expectedSettings.put(KEY_PRINTING_ANIMATION_TIME, 20);

        settings = new JSONSettings(FILE_PATH_CONSTRUCTOR);

        //when
        JSONObject settings = this.settings.getSettings();

        //then
        Assert.assertEquals(expectedSettings.toString(), settings.toString());
    }

    @Test
    public void shouldReturnDefaultSettingsWhenReadSettingsIsInvalid() {
        //given
        File file = new File(FILE_PATH);
        file.delete();
        writeInValidExampleSettingFile();
        JSONObject defaultSettings = settings.getDefaultSettings();
        settings = new JSONSettings(FILE_PATH_CONSTRUCTOR);

        //when
        JSONObject settings = this.settings.getSettings();

        //then
        Assert.assertEquals(defaultSettings.toString(), settings.toString());
    }

    @Test
    public void shouldReturnValidDataFromSettings() {
        //given
        File file = new File(FILE_PATH);
        file.delete();
        settings = new JSONSettings(FILE_PATH_CONSTRUCTOR);


        //when
        Boolean autoExit = settings.getSettingBoolean(KEY_AUTO_EXIT);
        Object printingTime = settings.getSettingInteger(KEY_PRINTING_ANIMATION_TIME);

        //then
        Assert.assertEquals(DEFAULT_AUTO_EXIT, autoExit);
        Assert.assertEquals(DEFAULT_PRINTING_ANIMATION_TIME, printingTime);

    }

    @Test
    public void shouldWriteSettingsToDefaultFile() {
        //given
        File file = new File(FILE_PATH);
        file.delete();
        settings = new JSONSettings(FILE_PATH_CONSTRUCTOR);
        file.delete();

        //when
        settings.writeCurrentSettingsToDefaultFile();

        //then
        Assert.assertTrue(file.exists());
        file.delete();

    }

    private void writeValidExampleSettingFile() {
        JSONObject expectedSettings = new JSONObject();
        expectedSettings.put(KEY_AUTO_EXIT, false);
        expectedSettings.put(KEY_PRINTING_ANIMATION_TIME, 20);

        try(BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_PATH))) {
            writer.write(expectedSettings.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeInValidExampleSettingFile() {
        JSONObject expectedSettings = new JSONObject();
        expectedSettings.put("wrong", false);
        expectedSettings.put(KEY_PRINTING_ANIMATION_TIME, 20);

        try(BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_PATH))) {
            writer.write(expectedSettings.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}