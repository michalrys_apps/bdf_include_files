package com.michalrys.bdfincludefiles;

import com.michalrys.bdfincludefiles.femfiles.MultipleFiles;
import com.michalrys.bdfincludefiles.femfiles.MultipleFilesImpl;
import com.michalrys.bdfincludefiles.view.WindowGuiView;

import java.awt.*;
import java.io.File;
import java.net.URISyntaxException;

public class SwingAppRun {
    public static String[] args;
    public static String appPath;
    public static String userName = System.getProperty("user.name");

    public static void main(String[] args) {
        setAppPath();
        SwingAppRun.args = args;
        MultipleFiles multipleFiles = new MultipleFilesImpl();

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new WindowGuiView(multipleFiles);
            }
        });
    }

    private static void setAppPath() {
        String path = "";
        try {
            path = new File(SwingAppRun.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        String lastPart = path.replaceFirst(".*\\\\", "");
        appPath = path.replaceFirst(lastPart, "");
    }
}