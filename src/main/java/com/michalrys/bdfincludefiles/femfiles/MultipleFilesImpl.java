package com.michalrys.bdfincludefiles.femfiles;

import com.michalrys.bdfincludefiles.MVC;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MultipleFilesImpl implements MultipleFiles, Observable {
    private List<MVC.View> views = new ArrayList<>();
    private AnyFemFile femFile;
    private String status;

    @Override
    public void workFileByFile(String[] args) {
        for (int i = 1; i <= args.length; i++) {
            File fileTemp = new File(args[i - 1]);

            setStatus(i + "/" + args.length + ": " + fileTemp.getName() + ": ");

            femFile = new AnyFemFile(fileTemp, views);
            femFile.set();

            setStatus("\t" + femFile.save() + "\n");
        }
    }

    @Override
    public void addObserver(MVC.View view) {
        if (!views.contains(view)) {
            views.add(view);
        }
    }

    @Override
    public void notifyAllObservers() {
        for (MVC.View view : views) {
            view.update(status);
        }
    }

    public void setStatus(String status) {
        this.status = status;
        notifyAllObservers();
    }
}
