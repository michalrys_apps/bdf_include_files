package com.michalrys.bdfincludefiles.femfiles.writer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class WriterAllInOne implements Writer {
    private final String FILE_NAME_POSTFIX;

    public WriterAllInOne(String postfix) {
        FILE_NAME_POSTFIX = postfix;
    }

    @Override
    public void write(StringBuffer contentToSave, String folderPath, String fileName) throws IOException {
        String fileNameToWrite = getFileToWrite(fileName);
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(folderPath + fileNameToWrite))) {
            bufferedWriter.write(contentToSave.toString());
        }
    }

    private String getFileToWrite(String fileName) {
        return fileName.replaceAll("\\.", (FILE_NAME_POSTFIX + "\\."));
    }
}