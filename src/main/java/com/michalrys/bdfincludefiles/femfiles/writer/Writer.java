package com.michalrys.bdfincludefiles.femfiles.writer;

import java.io.IOException;

public interface Writer {
    public void write(StringBuffer contentToSave, String folderPath, String fileName) throws IOException;
}
