package com.michalrys.bdfincludefiles.femfiles.file;

import java.io.File;

public class ExtendedFile extends File implements FileContainer {

    public ExtendedFile(String pathname) {
        super(pathname);
    }

    @Override
    public String getFileName() {
        return this.getName();
    }

    @Override
    public String getFilePath() {
        return this.getAbsolutePath();
    }

    @Override
    public String getFolderPath() {
        String absolutePath = this.getAbsolutePath();
        return absolutePath.replaceAll(this.getName(), "");
    }

    @Override
    public void deleteFile() {
        this.delete();
    }
}
