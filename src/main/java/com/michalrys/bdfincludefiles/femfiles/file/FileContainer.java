package com.michalrys.bdfincludefiles.femfiles.file;

public interface FileContainer {
    String getFilePath();

    String getFolderPath();

    String getFileName();

    void deleteFile();
}
