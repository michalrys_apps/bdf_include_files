package com.michalrys.bdfincludefiles.femfiles.reader;

import com.michalrys.bdfincludefiles.MVC;
import com.michalrys.bdfincludefiles.femfiles.Observable;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SimpleReaderImpl implements SimpleReader, Observable {
    private List<MVC.View> views = new ArrayList<>();
    private String status;

    @Override
    public StringBuffer get(String folderPath, String fileName) throws IOException {
        StringBuffer content = new StringBuffer();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(folderPath + fileName))) {
            setStatus("\t\tworking on: " + fileName);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line + "\n");
            }
        }
        content.replace(content.length() - 1, content.length(), "");
        setStatus("\t\t-> done.");
        return content;
    }

    @Override
    public void addObserver(MVC.View view) {
        if (!views.contains(view)) {
            views.add(view);
        }
    }

    @Override
    public void notifyAllObservers() {
        for (MVC.View view : views) {
            view.update(status);
        }
    }

    private void setStatus(String status) {
        this.status = status;
        notifyAllObservers();
    }
}