package com.michalrys.bdfincludefiles.femfiles.reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ComplexReaderMergeIncludeFiles implements ComplexReader {
    private SimpleReader simpleReader;
    private final static String MESSAGE_FOR_FILE_READING_EXCEPTION = "$ !!!ERROR: There was problem with reading a file: ";

    public ComplexReaderMergeIncludeFiles(SimpleReader simpleReader) {
        this.simpleReader = simpleReader;
    }

    @Override
    public StringBuffer get(String folderPath, String fileName) throws IOException {
        StringBuffer content = new StringBuffer();
        String line;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(folderPath + fileName))) {
            while ((line = bufferedReader.readLine()) != null) {
                if (line.toUpperCase().contains("INCLUDE") && line.contains("'")) {
                    String includeFileName = extractFileNameFromInclude(line);

                    try {
                        content.append(simpleReader.get(folderPath, includeFileName));
                    } catch (FileNotFoundException e) {
                        content.append(MESSAGE_FOR_FILE_READING_EXCEPTION + line);
                    }
                    content.append("\n");
                    continue;
                }
                content.append(line + "\n");
            }
        }
        content.replace(content.length() - 1, content.length(), "");

        return content;
    }

    private String extractFileNameFromInclude(String includeLine) {
        int first = includeLine.indexOf("'");
        return includeLine.substring(first + 1, includeLine.length() - 1);
    }
}