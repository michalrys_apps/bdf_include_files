package com.michalrys.bdfincludefiles.femfiles.reader;

import java.io.IOException;

public interface Reader {
    StringBuffer get(String folderPath, String fileName) throws IOException;
}
