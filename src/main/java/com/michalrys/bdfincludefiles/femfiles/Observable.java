package com.michalrys.bdfincludefiles.femfiles;

import com.michalrys.bdfincludefiles.MVC;

public interface Observable {
    void addObserver(MVC.View view);

    void notifyAllObservers();
}
