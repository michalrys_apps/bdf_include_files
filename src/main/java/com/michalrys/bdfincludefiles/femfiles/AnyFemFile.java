package com.michalrys.bdfincludefiles.femfiles;

import com.michalrys.bdfincludefiles.MVC;
import com.michalrys.bdfincludefiles.femfiles.file.ExtendedFile;
import com.michalrys.bdfincludefiles.femfiles.file.FileContainer;
import com.michalrys.bdfincludefiles.femfiles.reader.ComplexReaderMergeIncludeFiles;
import com.michalrys.bdfincludefiles.femfiles.reader.Reader;
import com.michalrys.bdfincludefiles.femfiles.reader.SimpleReader;
import com.michalrys.bdfincludefiles.femfiles.reader.SimpleReaderImpl;
import com.michalrys.bdfincludefiles.femfiles.writer.Writer;
import com.michalrys.bdfincludefiles.femfiles.writer.WriterAllInOne;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AnyFemFile implements FEMFile, Observable {
    private List<MVC.View> views = new ArrayList<>();
    private String status;
    private final FileContainer FILE;
    private StringBuffer fileContent;
    private Reader reader = new ComplexReaderMergeIncludeFiles(new SimpleReaderImpl());
    private Writer writer = new WriterAllInOne("_AllInOne");
    private final static String MESSAGE_FOR_FILE_READING_EXCEPTION = "$ !!!ERROR: There was problem with reading a file: ";
    private final static String MESSAGE_FOR_FILE_WRITING_EXCEPTION = "There was problem with writing a file. Please check it.";
    private final static String MESSAGE_FOR_FILE_READING_IO_EXCEPTION = "There was problem with I/O file operation.";
    private final static String MESSAGE_FOR_FILE_CORRECT_WRITING = "File was written successfully.";

    public AnyFemFile(String filePath) {
        FILE = new ExtendedFile(filePath);
    }

    public AnyFemFile(File file) {
        this.FILE = new ExtendedFile(file.getAbsolutePath());
    }

    public AnyFemFile(File file, List<MVC.View> views) {
        this.FILE = new ExtendedFile(file.getAbsolutePath());
        SimpleReader simpleReader = new SimpleReaderImpl();

        for(MVC.View view : views) {
            this.views.add(view);
            ((SimpleReaderImpl) simpleReader).addObserver(view);
        }
        reader = new ComplexReaderMergeIncludeFiles(simpleReader);
    }

    @Override
    public void setWriter(Writer writer) {
        this.writer = writer;
    }

    @Override
    public void setReader(Reader reader) {
        this.reader = reader;
    }

    @Override
    public StringBuffer readCompleteContent() {
        StringBuffer content = new StringBuffer();
        try {
            content = reader.get(FILE.getFolderPath(), FILE.getFileName());
        } catch (FileNotFoundException e) {
            content.append(MESSAGE_FOR_FILE_READING_EXCEPTION + FILE.getFileName());
            setStatus("\t! " + MESSAGE_FOR_FILE_READING_EXCEPTION + FILE.getFileName());
        } catch (IOException e) {
            content.append(MESSAGE_FOR_FILE_READING_IO_EXCEPTION);
            setStatus("\t->! " + MESSAGE_FOR_FILE_READING_IO_EXCEPTION);
        }
        setStatus("\t-> done.");
        return content;
    }

    @Override
    public String saveCompleteContentToNewFile(StringBuffer contentToSave) {
        try {
            writer.write(contentToSave, FILE.getFolderPath(), FILE.getFileName());
        } catch (IOException e) {
            setStatus("\t->! " + MESSAGE_FOR_FILE_WRITING_EXCEPTION);
            return MESSAGE_FOR_FILE_WRITING_EXCEPTION;
        }
        return MESSAGE_FOR_FILE_CORRECT_WRITING;
    }

    @Override
    public void set() {
        fileContent = readCompleteContent();
    }

    @Override
    public StringBuffer get() {
        return fileContent;
    }

    @Override
    public String save() {
        return saveCompleteContentToNewFile(fileContent);
    }

    @Override
    public void addObserver(MVC.View view) {
        if (!views.contains(view)) {
            views.add(view);
        }
    }

    @Override
    public void notifyAllObservers() {
        for (MVC.View view : views) {
            view.update(status);
        }
    }

    public void setStatus(String status) {
        this.status = status;
        notifyAllObservers();
    }
}