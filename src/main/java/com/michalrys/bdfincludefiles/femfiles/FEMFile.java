package com.michalrys.bdfincludefiles.femfiles;

import com.michalrys.bdfincludefiles.femfiles.reader.Reader;
import com.michalrys.bdfincludefiles.femfiles.writer.Writer;

public interface FEMFile {
    StringBuffer readCompleteContent();

    String saveCompleteContentToNewFile(StringBuffer contentToSave);

    void set();

    StringBuffer get();

    String save();

    void setWriter(Writer writer);

    void setReader(Reader reader);
}
