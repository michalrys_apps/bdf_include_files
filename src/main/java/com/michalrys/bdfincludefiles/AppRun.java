package com.michalrys.bdfincludefiles;

import com.michalrys.bdfincludefiles.controller.ControllerMultipleFilesAndSingleFile;
import com.michalrys.bdfincludefiles.femfiles.MultipleFiles;
import com.michalrys.bdfincludefiles.femfiles.MultipleFilesImpl;

public class AppRun {
    public static void main(String[] args) {
        MultipleFiles multipleFiles = new MultipleFilesImpl();
        ControllerMultipleFilesAndSingleFile controllerMultipleFilesAndSingleFile = new ControllerMultipleFilesAndSingleFile(multipleFiles);

        controllerMultipleFilesAndSingleFile.showAppInfo();
        controllerMultipleFilesAndSingleFile.runWithUserArgs(args);
        controllerMultipleFilesAndSingleFile.showAppEnd();
    }
}
