package com.michalrys.bdfincludefiles.controller;

import com.michalrys.bdfincludefiles.MVC;
import com.michalrys.bdfincludefiles.femfiles.MultipleFiles;
import com.michalrys.bdfincludefiles.femfiles.Observable;
import com.michalrys.bdfincludefiles.view.SimpleConsoleView;
import com.michalrys.bdfincludefiles.view.swingwindows.settings.GuiSettings;

public class ControllerMultipleFilesAndSingleFile implements MVC.Controller {
    private MultipleFiles multipleFiles;
    private MVC.View view;

    public ControllerMultipleFilesAndSingleFile(MultipleFiles multipleFiles) {
        this.multipleFiles = multipleFiles;
        view = new SimpleConsoleView(this, multipleFiles);
        ((Observable) multipleFiles).addObserver(view);
    }

    @Override
    public void showAppInfo() {
        view.printAppInfo();
    }

    @Override
    public void runWithUserArgs(String[] args) {
        multipleFiles.workFileByFile(args);
    }

    @Override
    public void showAppEnd() {
        view.printAppEnd();
    }

    @Override
    public Runnable runWithUserArgsInOtherThread(String[] args) {
        return null;
    }

    @Override
    public void setGuiSettings(GuiSettings guiSettings) {
    }
}
