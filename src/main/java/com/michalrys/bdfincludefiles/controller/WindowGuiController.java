package com.michalrys.bdfincludefiles.controller;

import com.michalrys.bdfincludefiles.MVC;
import com.michalrys.bdfincludefiles.femfiles.MultipleFiles;
import com.michalrys.bdfincludefiles.view.WindowGuiView;
import com.michalrys.bdfincludefiles.view.swingwindows.settings.GuiSettings;

import java.awt.*;

public class WindowGuiController implements MVC.Controller {
    private MultipleFiles model;
    private MVC.View view;
    private GuiSettings guiSettings;
    private static final String SETTINGS_AUTO_EXIT = "autoExit";

    public WindowGuiController(MultipleFiles multipleFiles, MVC.View view) {
        this.model = multipleFiles;
        this.view = view;
    }

    @Override
    public void setGuiSettings(GuiSettings guiSettings) {
        this.guiSettings = guiSettings;
    }

    @Override
    public void runWithUserArgs(String[] args) {
        ((WindowGuiView)view).setEnablePrintingButtons(false);
        model.workFileByFile(args);
        ((WindowGuiView)view).setEnablePrintingButtons(true);
    }

    @Override
    public void showAppInfo() {

    }

    @Override
    public void showAppEnd() {

    }

    @Override
    public Runnable runWithUserArgsInOtherThread(String[] args) {
        return new Runnable() {
            @Override
            public void run() {
                ((WindowGuiView)view).setEnablePrintingButtons(false);
                model.workFileByFile(args);
                if(guiSettings.getSettingBoolean(SETTINGS_AUTO_EXIT)) {
                    view.printAppEnd();
                }
                ((WindowGuiView)view).setEnablePrintingButtons(true);
            }
        };
    }
}
