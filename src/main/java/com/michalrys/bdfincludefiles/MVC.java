package com.michalrys.bdfincludefiles;

import com.michalrys.bdfincludefiles.femfiles.AnyFemFile;
import com.michalrys.bdfincludefiles.view.swingwindows.settings.GuiSettings;

public interface MVC {
    interface Controller {

        void runWithUserArgs(String[] args);

        void showAppInfo();

        void showAppEnd();

        Runnable runWithUserArgsInOtherThread(String[] args);

        void setGuiSettings(GuiSettings guiSettings);
    }

    interface View {
        void runCalculationWithUserArgs();

        void printAppInfo();

        void printOutStatus();

        void update(String status);

        void setFemFile(AnyFemFile femFile);

        void printAppEnd();
    }
}