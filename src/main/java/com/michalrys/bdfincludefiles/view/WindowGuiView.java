package com.michalrys.bdfincludefiles.view;

import com.michalrys.bdfincludefiles.MVC;
import com.michalrys.bdfincludefiles.SwingAppRun;
import com.michalrys.bdfincludefiles.controller.WindowGuiController;
import com.michalrys.bdfincludefiles.femfiles.AnyFemFile;
import com.michalrys.bdfincludefiles.femfiles.FEMFile;
import com.michalrys.bdfincludefiles.femfiles.MultipleFiles;
import com.michalrys.bdfincludefiles.femfiles.Observable;
import com.michalrys.bdfincludefiles.view.swingwindows.StatusPreviewWindow;
import com.michalrys.bdfincludefiles.view.swingwindows.filedrop.FileDrop;
import com.michalrys.bdfincludefiles.view.tools.AppInfo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class WindowGuiView extends JFrame implements MVC.View {
    private MVC.Controller controller;
    private MultipleFiles multipleFiles;
    private FEMFile anyFemFile;
    private static AppInfo appInfo = new AppInfo(25, "BDF Include Files", "1.0.0", "Michał Ryś", "2019-05-06");
    private StatusPreviewWindow statusPreviewWindow;
    private String status = "";
    private int statusLines = 0;
    private static final int STATUS_MAX_LINES = 10;
    private static final int TIME_LETTERS_PRITING_ANIMATION_FOR_HEADER_MILISEC = 20;
    private static final String SETTINGS_AUTO_EXIT = "autoExit";
    private static final String SETTINGS_PRINTING_ANIMATION_TIME = "printingAnimationTime";

    public WindowGuiView(MultipleFiles multipleFiles) throws HeadlessException {
        this.multipleFiles = multipleFiles;
        ((Observable) multipleFiles).addObserver(this);
        controller = new WindowGuiController(this.multipleFiles, this);
        statusPreviewWindow = new StatusPreviewWindow();
        controller.setGuiSettings(statusPreviewWindow.guiSettings);

        setUpWindowsSettings();
        initializeActionListeners();

        printAppInfo();
        runCalculationWithUserArgs();
    }

    private void setUpWindowsSettings() {
        Boolean autoExit = statusPreviewWindow.guiSettings.getSettingBoolean(SETTINGS_AUTO_EXIT);
        if (autoExit) {
            statusPreviewWindow.buttonAutoExitStatus.setText("AUTOEXIT ON");
            statusPreviewWindow.buttonAutoExitStatus.setBackground(statusPreviewWindow.bg3active);
        } else {
            statusPreviewWindow.buttonAutoExitStatus.setText("AUTOEXIT OFF");
            statusPreviewWindow.buttonAutoExitStatus.setBackground(statusPreviewWindow.bg);
        }
    }

    private void initializeActionListeners() {
        statusPreviewWindow.buttonExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printAppEnd();
            }
        });

        statusPreviewWindow.buttonAutoExitStatus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Boolean autoExit = statusPreviewWindow.guiSettings.getSettingBoolean(SETTINGS_AUTO_EXIT);
                if (autoExit) {
                    statusPreviewWindow.buttonAutoExitStatus.setText("AUTOEXIT OFF");
                    statusPreviewWindow.buttonAutoExitStatus.setBackground(statusPreviewWindow.bg);
                    statusPreviewWindow.guiSettings.setSettingBoolean(SETTINGS_AUTO_EXIT, false);
                    statusPreviewWindow.guiSettings.writeCurrentSettingsToDefaultFile();
                } else {
                    statusPreviewWindow.buttonAutoExitStatus.setText("AUTOEXIT ON");
                    statusPreviewWindow.buttonAutoExitStatus.setBackground(statusPreviewWindow.bg3active);
                    statusPreviewWindow.guiSettings.setSettingBoolean(SETTINGS_AUTO_EXIT, true);
                    statusPreviewWindow.guiSettings.writeCurrentSettingsToDefaultFile();
                }
            }
        });

        statusPreviewWindow.buttonClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                status = "";
                statusLines = 0;
                printOutStatus();
            }
        });

        statusPreviewWindow.buttonInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String header = appInfo.getHeader();
                String remark = appInfo.getRemark(93, "~", "" +
                        "    This small program find sentence with INCLUDE \"file.bdf\" and replace it by " +
                        "content of the file \"file.bdf\". This works for Nastran and Optistruct " +
                        "input files.");
                String version100 = appInfo.getRemark(83, "V1.0.0",
                        "1st rel - first release.");
                String version004 = appInfo.getRemark(83, "V0.0.4",
                        "4th beta - implementation of drag and drop.");
                        //"4th beta - implementation of FileDrop.");
                String version003 = appInfo.getRemark(83, "V0.0.3",
                        "3rd beta - gui experiments.");
                String version002 = appInfo.getRemark(83, "V0.0.2",
                        "2nd beta - ***.");
                        //"2nd beta - mvc, deep observable mode.");
                String version001 = appInfo.getRemark(83, "V0.0.1",
                        "1st beta - ***.");
                        //"1st beta - for validation test only.");
                StringBuffer infoMessage = new StringBuffer();

                infoMessage.append(header + "\n");
                infoMessage.append(remark + "\n");
                infoMessage.append(version100 + "\n");
                infoMessage.append(version004 + "\n");
                infoMessage.append(version003 + "\n");
                infoMessage.append(version002 + "\n");
                infoMessage.append(version001 + "\n");
                infoMessage.replace(infoMessage.length() - 1, infoMessage.length(), "");

                status = "";
                statusLines = 0;
                printOutStatus();

                Thread printingThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        setEnablePrintingButtons(false);
                        for (int i = 0; i < infoMessage.length(); i++) {
                            char letter = infoMessage.charAt(i);
                            try {
                                Thread.sleep(statusPreviewWindow.guiSettings.getSettingInteger(SETTINGS_PRINTING_ANIMATION_TIME));
                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                            }

                            String currentStatus = statusPreviewWindow.status.getText();
                            statusPreviewWindow.status.setText(currentStatus + letter);
                        }
                        setEnablePrintingButtons(true);
                    }
                });
                printingThread.start();
            }
        });

        statusPreviewWindow.buttonAuthor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                status = "";
                statusLines = 0;
                printOutStatus();

                StringBuffer infoMessage = new StringBuffer();
                infoMessage.append("\n" +
                        "  Author: Michał Ryś\n" +
                        "  E-mail: michalrys@gmail.com\n" +
                        " License: Freeware for EC-E employees.");

                Thread printingThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        setEnablePrintingButtons(false);
                        for (int i = 0; i < infoMessage.length(); i++) {
                            char letter = infoMessage.charAt(i);
                            try {
                                Thread.sleep(statusPreviewWindow.guiSettings.getSettingInteger(SETTINGS_PRINTING_ANIMATION_TIME));
                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                            }

                            String currentStatus = statusPreviewWindow.status.getText();
                            statusPreviewWindow.status.setText(currentStatus + letter);
                        }
                        setEnablePrintingButtons(true);
                    }
                });
                printingThread.start();
            }
        });

        new FileDrop(statusPreviewWindow.status, new FileDrop.Listener() {
            @Override
            public void filesDropped(File[] files) {
                String args[] = new String[files.length];
                for (int i = 0; i < files.length; i++) {
                    args[i] = files[i].getPath();
                }
                Thread thread = new Thread(controller.runWithUserArgsInOtherThread(args));
                thread.start();
            }
        });
    }

    public void setEnablePrintingButtons(boolean setToEnabled) {
        List<JButton> buttonList = new ArrayList<>();
        buttonList.add(statusPreviewWindow.buttonAuthor);
        buttonList.add(statusPreviewWindow.buttonClear);
        buttonList.add(statusPreviewWindow.buttonInfo);

        for (JButton button : buttonList) {
            button.setEnabled(setToEnabled);
        }
    }

    @Override
    public void printAppInfo() {

        Thread printAppInfoThread = new Thread(new Runnable() {
            @Override
            public void run() {
                StringBuffer messageToPrint = new StringBuffer();
                messageToPrint.append("\nBDF Include ");

                for (int i = 1; i <= 5; i++) {
                    try {
                        Thread.sleep(TIME_LETTERS_PRITING_ANIMATION_FOR_HEADER_MILISEC);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    StringBuffer messageF = new StringBuffer();
                    StringBuffer messageI = new StringBuffer();
                    StringBuffer messageL = new StringBuffer();
                    StringBuffer messageE = new StringBuffer();
                    StringBuffer messageS = new StringBuffer();
                    messageF.append("      F");
                    messageI.append("     i");
                    messageL.append("    l");
                    messageE.append("   e");
                    messageS.append("  s");
                    String message = "";
                    if (i == 1) {
                        animationText(100, messageToPrint, 5, messageF, message);
                    } else if (i == 2) {
                        messageToPrint.append("F");
                        animationText(100, messageToPrint, 4, messageI, message);
                    } else if (i == 3) {
                        messageToPrint.append("i");
                        animationText(100, messageToPrint, 3, messageL, message);
                    } else if (i == 4) {
                        messageToPrint.append("l");
                        animationText(100, messageToPrint, 2, messageE, message);
                    } else if (i == 5) {
                        messageToPrint.append("e");
                        animationText(100, messageToPrint, 2, messageS, message);
                    }
                }
            }
        });
        printAppInfoThread.start();
    }

    private void animationText(int milisec, StringBuffer massageConstantBase, int removeSpaces, StringBuffer messageLetter, String messageFinalEffect) {
        for (int j = removeSpaces; j >= 1; j--) {
            try {
                Thread.sleep(milisec);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            messageLetter.replace(0, 1, "");
            messageFinalEffect = massageConstantBase.toString() + messageLetter.toString();
            statusPreviewWindow.intro.setText(messageFinalEffect + "\n");
        }
    }

    @Override
    public void runCalculationWithUserArgs() {
        Thread thread = new Thread(controller.runWithUserArgsInOtherThread(SwingAppRun.args));
        thread.start();
    }

    @Override
    public void printOutStatus() {
        statusPreviewWindow.status.setText(status);
    }

    @Override
    public void update(String status) {
        if (this.status.equals("")) {
            this.status = status;
            statusLines++;
        } else {
            this.status = this.status + "\n" + status;
            statusLines++;
        }
        reduceStatusIfReachMaxStatusLine();
        printOutStatus();
    }

    private void reduceStatusIfReachMaxStatusLine() {
        if (statusLines >= STATUS_MAX_LINES) {
            status = status.replaceFirst(".*\\n", "");
        }
    }

    @Override
    public void setFemFile(AnyFemFile femFile) {
        anyFemFile = femFile;
    }

    @Override
    public void printAppEnd() {
        update("Goodbye " + SwingAppRun.userName + ". Have a nice day. Start closing (5 seconds):");

        Thread closingApp = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i <= 5; i++) {
                    try {
                        Thread.sleep(1000);
                        update("- - - - - - - - - - " + i + " - - - - - - - - - -");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.exit(0);
            }
        });
        closingApp.start();
    }
}
