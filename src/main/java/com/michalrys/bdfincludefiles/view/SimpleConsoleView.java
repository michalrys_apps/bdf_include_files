package com.michalrys.bdfincludefiles.view;

import com.michalrys.bdfincludefiles.MVC;
import com.michalrys.bdfincludefiles.femfiles.AnyFemFile;
import com.michalrys.bdfincludefiles.femfiles.FEMFile;
import com.michalrys.bdfincludefiles.femfiles.MultipleFiles;
import com.michalrys.bdfincludefiles.view.tools.*;

public class SimpleConsoleView implements com.michalrys.bdfincludefiles.MVC.View {
    private static AppInfo appInfo = new AppInfo(25, "BDF Include Files", "0.0.2", "Michał Ryś", "2019-04-24");
    private MVC.Controller controller;
    private MultipleFiles multipleFiles;
    private FEMFile anyFemFile;
    private String status;

    public SimpleConsoleView(MVC.Controller controller, MultipleFiles multipleFiles) {
        this.controller = controller;
        this.multipleFiles = multipleFiles;
    }

    @Override
    public void setFemFile(AnyFemFile femFile) {
        anyFemFile = femFile;
    }

    @Override
    public void update(String status) {
        this.status = status;
        printOutStatus();
    }

    @Override
    public void printOutStatus() {
        System.out.println(status);
    }

    @Override
    public void printAppEnd() {
        System.out.println("Have a nice day.");
    }

    @Override
    public void runCalculationWithUserArgs() {
        //TODO added to window view
    }

    @Override
    public void printAppInfo() {
        String header = appInfo.getHeader();
        String remark = appInfo.getRemark(74, "~", "" +
                "This is a console application. " +
                "This tool merge into main bdf file with the include definitions, " +
                "extra bdf files mentioned in INCLUDE '...file...'.");
        String version002 = appInfo.getRemark(64, "V0.0.2",
                "2nd beta - mvc, deep observable mode.");
        String version001 = appInfo.getRemark(64, "V0.0.1",
                "1st beta - for validation test only.");
        System.out.println(header);
        System.out.println(remark);
        System.out.println(version002);
        System.out.println(version001);
        System.out.println("");
    }
}
