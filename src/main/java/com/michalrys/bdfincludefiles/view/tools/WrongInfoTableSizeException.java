package com.michalrys.bdfincludefiles.view.tools;

class WrongInfoTableSizeException extends RuntimeException {
    WrongInfoTableSizeException(int length, String text) {
        super("Given text = '" + text + "' is longer than table length = '" + length + "'.");
    }
}
