package com.michalrys.bdfincludefiles.view.tools;

public class AppInfo {

    private int entryLength;
    private final String TABLE_CORNER = "+";
    private final String TABLE_COLUMN = "|";
    private final String SEPARATION = " ";
    private final String TABLE_ROW = "-";
    private final String DEFAULT_AUTHOR = "Michał Ryś";
    private final String DEFAULT_VERSION = "0.0.1";
    private final String DEFAULT_DATE = "2019.04.16";
    private final String DEFAULT_TITLE = "BDF Include Files";
    private String title;
    private String author;
    private String version;
    private String date;

    public AppInfo(int entryLength) {
        title = DEFAULT_TITLE;
        author = "Author: " + DEFAULT_AUTHOR;
        version = "Version: " + DEFAULT_VERSION;
        date = "Date: " + DEFAULT_DATE;
        this.entryLength = entryLength;
    }

    public AppInfo(int entryLength, String title, String version, String author, String date) {
        this.entryLength = entryLength;
        this.title = title;
        this.author = "Author: " + author;
        this.version = "Version: " + version;
        this.date = "Date: " + date;
    }

    public String getHeader() {
        StringBuffer result = new StringBuffer();

        result.append(getRow(RowType.SOLID));
        result.append(getTextInTable(title));
        result.append(getRow(RowType.HALF_SOLID));
        result.append(getTextInTable(version));
        result.append(getTextInTable(author));
        result.append(getTextInTable(date));
        result.append(getRow(RowType.SOLID));

        result.replace(result.length() - 1, result.length(), "");
        return result.toString();
    }

    public String getRemark(int length, String decorator, String text) {
        int howManyRows = text.length() / length;
        howManyRows = text.length() % length != 0 ? howManyRows + 1 : howManyRows;
        StringBuffer result = new StringBuffer();

        int start = 0;
        int end = text.length() <= length ? start + text.length() : start + length;

        for (int i = 1; i <= howManyRows; i++) {
            result.append(decorator + SEPARATION + text.substring(start, end));

            if(end == text.length()) {
                for (int j = end; j < howManyRows * length; j++) {
                    result.append(" ");
                }
            }

            result.append(SEPARATION + decorator + "\n");

            start = end;
            end = end + length <= text.length() ? end + length : text.length();
        }
        result.replace(result.length() - 1, result.length(), "");
        return result.toString();
    }

    private StringBuffer getRow(RowType rowType) {
        StringBuffer result = new StringBuffer();
        String boundaryRowStart;
        String boundaryRowEnd;
        if (rowType.equals(RowType.HALF_SOLID)) {
            boundaryRowStart = SEPARATION;
            boundaryRowEnd = entryLength % 2 == 0 ? TABLE_ROW : SEPARATION;
        } else {
            boundaryRowStart = TABLE_ROW;
            boundaryRowEnd = TABLE_ROW;
        }

        result.append(TABLE_CORNER + boundaryRowStart);
        for (int i = 1; i <= entryLength; i++) {
            if (rowType.equals(RowType.HALF_SOLID)) {
                result = i % 2 == 0 ? result.append(SEPARATION) : result.append(TABLE_ROW);
            } else {
                result.append(TABLE_ROW);
            }
        }
        result.append(boundaryRowEnd + TABLE_CORNER + "\n");
        return result;
    }

    private StringBuffer getTextInTable(String text) throws WrongInfoTableSizeException {
        StringBuffer result = new StringBuffer();
        result.append(TABLE_COLUMN + SEPARATION);
        if (text.length() > entryLength) {
            throw new WrongInfoTableSizeException(entryLength, text);
        }
        result.append(text);
        for (int i = text.length(); i < entryLength; i++) {
            result.append(SEPARATION);
        }
        result.append(SEPARATION + TABLE_COLUMN + "\n");
        return result;
    }
}