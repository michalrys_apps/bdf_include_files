package com.michalrys.bdfincludefiles.view.tools;

public enum RowType {
    SOLID,
    HALF_SOLID;
}
