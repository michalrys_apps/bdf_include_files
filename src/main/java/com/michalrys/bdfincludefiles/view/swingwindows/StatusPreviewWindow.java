package com.michalrys.bdfincludefiles.view.swingwindows;

import com.michalrys.bdfincludefiles.SwingAppRun;
import com.michalrys.bdfincludefiles.view.swingwindows.filedrop.FileDrop;
import com.michalrys.bdfincludefiles.view.swingwindows.settings.GuiSettings;
import com.michalrys.bdfincludefiles.view.swingwindows.settings.JSONSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.dnd.DropTarget;
import java.io.File;

public class StatusPreviewWindow extends JFrame {
    public GuiSettings guiSettings;
    public final Color bg = new Color(0x000000);
    //private final Color bg2 = new Color(0x000042);
    public final Color bg2 = new Color(0x0F0F0F);
    public final Color bg3active = new Color(0x004000);
    private final Color colorText1 = new Color(0x009916);
    private final Font textPrint1 = new Font("Consolas", NORMAL, 10);
    private final Font textPrint2 = new Font("Consolas", NORMAL, 8);
    private final Font textButtons = new Font("Arial", Font.BOLD, 8);
    private final Cursor cursorHand = new Cursor(Cursor.HAND_CURSOR);
    public JPanel mainPanel;
    public JTextArea intro;
    public JTextArea status = new JTextArea();
    public JButton buttonExit;
    public JButton buttonAutoExitStatus;
    public JButton buttonInfo;
    public JButton buttonClear;
    public JButton buttonAuthor;

    public StatusPreviewWindow() throws HeadlessException {
        guiSettings = new JSONSettings(SwingAppRun.appPath);

        setTitle("BDF include files");
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setSize(new Dimension(610, 270));
        //setMinimumSize(new Dimension(610, 270));
        setResizable(false);
        setLocation(150, 100);

        BorderLayout borderLayout = new BorderLayout();
        mainPanel = new JPanel(borderLayout);
        mainPanel.setBackground(bg);

        intro = new JTextArea();
        intro.setMargin(new Insets(0, 5, 0, 5));
        intro.setFont(textPrint1);
        intro.setBackground(bg);
        intro.setForeground(colorText1);
        intro.setLineWrap(true);
        intro.setEditable(false);
        intro.setAutoscrolls(true);
        intro.setText("");

        status.setFont(textPrint1);
        status.setBackground(bg2);
        status.setForeground(colorText1);
        status.setMargin(new Insets(0, 5, 0, 5));
        status.setLineWrap(true);
        status.setEditable(false);
        status.setSize(300, 10);
        status.setText("Drag and drop any file or files here.");
        //status.setDragEnabled(true);  WE DO NOT NEED THIS WHEN FILEDROP IS IN USE

        JPanel footer = new JPanel();
        FlowLayout flowLayout = new FlowLayout();
        flowLayout.setAlignment(FlowLayout.RIGHT);
        flowLayout.setHgap(5);
        flowLayout.setVgap(5);
        footer.setLayout(flowLayout);
        footer.setBackground(bg);

        JTextArea footerLabel = new JTextArea("actions: ");
        footerLabel.setBackground(bg);
        footerLabel.setFont(textPrint2);
        footerLabel.setForeground(colorText1);
        footerLabel.setEditable(false);

        buttonExit = new JButton("EXIT");
        buttonExit.setCursor(cursorHand);
        buttonExit.setPreferredSize(new Dimension(50, 15));
        buttonExit.setMargin(new Insets(0, 0, 0, 0));
        buttonExit.setForeground(colorText1);
        buttonExit.setBackground(bg);
        buttonExit.setFont(textButtons);

        buttonAutoExitStatus = new JButton("AUTOEXIT ON");
        buttonAutoExitStatus.setCursor(cursorHand);
        buttonAutoExitStatus.setPreferredSize(new Dimension(70, 15));
        buttonAutoExitStatus.setMargin(new Insets(0, 0, 0, 0));
        buttonAutoExitStatus.setForeground(colorText1);
        buttonAutoExitStatus.setBackground(bg3active);
        buttonAutoExitStatus.setFont(textButtons);

        buttonClear = new JButton("CLEAR");
        buttonClear.setCursor(cursorHand);
        buttonClear.setPreferredSize(new Dimension(50, 15));
        buttonClear.setMargin(new Insets(0, 0, 0, 0));
        buttonClear.setForeground(colorText1);
        buttonClear.setBackground(bg);
        buttonClear.setFont(textButtons);

        buttonInfo = new JButton("INFO");
        buttonInfo.setCursor(cursorHand);
        buttonInfo.setPreferredSize(new Dimension(50, 15));
        buttonInfo.setMargin(new Insets(0, 0, 0, 0));
        buttonInfo.setForeground(colorText1);
        buttonInfo.setBackground(bg);
        buttonInfo.setFont(textButtons);

        buttonAuthor = new JButton("AUTHOR");
        buttonAuthor.setCursor(cursorHand);
        buttonAuthor.setPreferredSize(new Dimension(50, 15));
        buttonAuthor.setMargin(new Insets(0, 0, 0, 0));
        buttonAuthor.setForeground(colorText1);
        buttonAuthor.setBackground(bg);
        buttonAuthor.setFont(textButtons);

        footer.add(footerLabel);
        footer.add(buttonAutoExitStatus);
        footer.add(buttonClear);
        footer.add(buttonInfo);
        footer.add(buttonAuthor);
        footer.add(buttonExit);

        mainPanel.add(BorderLayout.NORTH, intro);
        mainPanel.add(BorderLayout.CENTER, status);
        mainPanel.add(BorderLayout.SOUTH, footer);

        add(mainPanel);
    }
}