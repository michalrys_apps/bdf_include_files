package com.michalrys.bdfincludefiles.view.swingwindows.settings;

import org.json.JSONObject;

import java.io.*;
import java.util.Set;

public class JSONSettings implements GuiSettings {
    private static final String KEY_AUTO_EXIT = "autoExit";
    private static final String KEY_PRINTING_ANIMATION_TIME = "printingAnimationTime";
    private static final boolean DEFAULT_AUTO_EXIT = false;
    private static final int DEFAULT_PRINTING_ANIMATION_TIME = 8;
    private JSONObject settings;
    private JSONObject defaultSettings;
    private String fileName = "BDFIncludeSettings.json";
    private String filePath; //previously there was always on D:\


    public JSONSettings(String filePath) {
        generateDefaultSettings();
        setDefaultSettings();
        this.filePath = filePath + fileName;

        if(doesFileSettingsExist(this.filePath)) {
            setSettingsFromFile(this.filePath);
        } else {
            settings = defaultSettings;
        }
        writeSettingsToFile(this.filePath);
    }

    @Override
    public boolean doesFileSettingsExist(String filePath) {
        return new File(filePath).exists();
    }

    @Override
    public void generateDefaultSettings() {
        defaultSettings = new JSONObject();
        defaultSettings.put(KEY_AUTO_EXIT, DEFAULT_AUTO_EXIT);
        defaultSettings.put(KEY_PRINTING_ANIMATION_TIME, DEFAULT_PRINTING_ANIMATION_TIME);
    }

    @Override
    public void setDefaultSettings() {
        settings = defaultSettings;
    }

    @Override
    public void setSettingsFromFile(String filePath) {
        if (!doesFileSettingsExist(this.filePath)) {
            settings = defaultSettings;
        }

        StringBuffer fileContent = new StringBuffer();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                fileContent.append(line + "\n");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject fileContentAsJson = new JSONObject(fileContent.toString());

        if(isJSONValid(fileContentAsJson)) {
            settings = fileContentAsJson;
        } else {
            settings = defaultSettings;
        }
    }

    @Override
    public boolean isJSONValid(JSONObject json) {
        Set<String> keys = json.keySet();
        if(!keys.contains(KEY_AUTO_EXIT)) {
            return false;
        }

        if(!keys.contains(KEY_PRINTING_ANIMATION_TIME)) {
            return false;
        }
        return true;
    }

    @Override
    public void writeSettingsToFile(String filePath) {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            writer.write(settings.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void writeCurrentSettingsToDefaultFile() {
        writeSettingsToFile(filePath);
    }

    @Override
    public JSONObject getDefaultSettings() {
        return defaultSettings;
    }

    @Override
    public JSONObject getSettings() {
        return settings;
    }

    @Override
    public void setSettingBoolean(String key, Boolean value) {
        settings.put(key, value);
    }

    @Override
    public void setSettingInteger(String key, Integer value) {
        settings.put(key, value);
    }

    @Override
    public Object getSetting(String key) {
        return settings.get(key);
    }

    @Override
    public Boolean getSettingBoolean(String key) {
        return settings.getBoolean(key);
    }

    @Override
    public Integer getSettingInteger(String key) {
        return settings.getInt(key);
    }
}
