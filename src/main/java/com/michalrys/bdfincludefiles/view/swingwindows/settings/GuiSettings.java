package com.michalrys.bdfincludefiles.view.swingwindows.settings;

import org.json.JSONObject;

public interface GuiSettings {
    boolean doesFileSettingsExist(String filePath);

    void generateDefaultSettings();

    void setDefaultSettings();

    void setSettingsFromFile(String filePath);

    boolean isJSONValid(JSONObject json);

    void writeSettingsToFile(String filePath);

    void writeCurrentSettingsToDefaultFile();

    JSONObject getDefaultSettings();

    JSONObject getSettings();

    Object getSetting(String key);

    Boolean getSettingBoolean(String key);

    Integer getSettingInteger(String key);

    void setSettingBoolean(String key, Boolean value);

    void setSettingInteger(String key, Integer value);
}
